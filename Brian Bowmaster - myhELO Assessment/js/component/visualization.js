/**
 * Visualize the results of an API call to the FDA.
 */
component.visualization = function() {
  component.apply(this, arguments);
};
assessment.extend(component.visualization, component);

/**
 * Decorate.
 *
 * @param {HTMLDivElement} parent
 */
component.visualization.prototype.decorate = function(parent) {
  _this = this;
  var container = document.createElement('div');
  container.setAttribute('id','container');
  parent.appendChild(container);
  
  // Select bones
  var boneList = document.createElement('select');
  boneList.setAttribute('id','ProsthesisTypes');
  boneList.setAttribute('multiple','multiple');
  boneList.setAttribute('style','width: 280px');
  var types=[['select','Select Prosthesis Types (<ctrl> for multiple)'],['Ankle','Ankle Replacement'],['Elbow','Elbow Replacement'],['Hip','Hip Replacement'],['Knee','Knee Replacement'],['Rib','Rib Replacement'],['Shoulder','Shoulder Replacement'],['Wrist','Wrist Replacement']];
  for (var i=0; i<types.length; i++) {
    var item = document.createElement('option');
    item.setAttribute('value',types[i][0]);
    item.text = types[i][1];
    if (i==0) {
      item.setAttribute('disabled','');
    } else if (i==1) {
      item.setAttribute('selected','');
    }
    boneList.appendChild(item);
  }
  container.appendChild(boneList);
    
  // Select events
  var eventList = document.createElement('select');
  eventList.setAttribute('id','EventTypes');
  eventList.setAttribute('multiple','multiple');
  eventList.setAttribute('style','width: 280px');
  var types=[['select','Select Event Types (<ctrl> for multiple)'],['death','Deaths'],['injury','Injuries'],['malfunction','Malfunctions']];
  for (var i=0; i<types.length; i++) {
    var item = document.createElement('option');
    item.setAttribute('value',types[i][0]);
    item.text = types[i][1];
    if (i==0) {
      item.setAttribute('disabled','');
    } else {
      item.setAttribute('selected','');
    }
    eventList.appendChild(item);
  }
  container.appendChild(eventList);
  
  // Update button
  container.appendChild(document.createElement('p'));
  var buttonEle = document.createElement('button');
  buttonEle.setAttribute('onclick','newGraph()');
  buttonEle.appendChild(document.createTextNode('Update Graph'));
  container.appendChild(buttonEle);
  container.appendChild(document.createElement('p'));
  
  // Build the chart
  var chartEle = document.createElement('canvas');
  chartEle.setAttribute('id','eventChart1');
  chartEle.setAttribute('width','1200');
  chartEle.setAttribute('height','600');
  
  chart = new Chart(chartEle, {
    type: 'line',
    options: {
      responsive: false,
	  legend: {position: 'right'}
    }
  });  
  container.appendChild(chartEle);  

  // Source link at the bottom
  var sourceText = document.createElement('div');
  sourceText.innerHTML = 'All data is drawn from the openFDA API. See '+'https://open.fda.gov/'.link('https://open.fda.gov/')+' for more information';
  sourceText.style.position = 'absolute';
  sourceText.style.bottom = '0px';
  sourceText.style.textAlign = 'center';
  sourceText.style.width = '100%';
  document.body.appendChild(sourceText);
  
  // Prepare the initial graph
  urls = getURLs([['Ankle Replacement','Ankle']],[['Deaths','death'],['Injuries','injury'],['Malfunctions','malfunction']]);
  makeQueries(urls,_this);
};

/**
 * Transforms data returned by an API call into a count of events per year.
 * Once all queries are complete, generates the graph of all data.
 *
 * @param {HTMLElement} parent
 */
component.visualization.prototype.prep_data = function(parent) {
  // Get parent if this was called by the Update Graph button
  if (typeof parent !== 'undefined') {
    var parent = document.getElementById('container');
  }
  
  // If a query returns no results, we'll still show a line of 0's
  var dataLen = 0;
  if (typeof _this.data !== 'undefined') {
    dataLen = _this.data.length;
  }
  
  // Prep variables
  var eventCt = 0;
  var ctByYear = [];
  var currentYear = 2000; // Start with 2000 even if the query doesn't return results until later years. 0's will be prepended  
  
  // Loop through events. 
  // Events are returned in ascending date order, so we are able to keep count for the year as a single variable.
  for (var i=0; i<dataLen; i++) {
    day = _this.data[i];
    year = parseInt(day.time.slice(0,4));
    
    if (year!=currentYear) {
      // This day is in a new year. Store the old year's data
      ctByYear.push(eventCt);
      
      // Now store 0's for any missing years
      for (var j=currentYear+1; j<year; j++) {
        ctByYear.push(0);
      }
      
      // Prep for the new year
      currentYear = year;
      eventCt = 0
    }
    
    // Wait to update the count in case this was the first day of the year
    eventCt += day.count;
  }
  // Save data for the final year of results from the query
  ctByYear.push(eventCt);
  
  // Store 0's for any remaining years
  var thisYear = getTodayStr(true);
  if (currentYear < thisYear) {
    for (i=currentYear; i<=thisYear; i++) {
      ctByYear.push(0);
    }
  }
  
  // Store data for the chart
  events[type] = {data: ctByYear, label: type};
  
  // This part feels sketchy and could be subject to a race condition.
  // Another option would be to "mark" the last query and force its prep_data call to wait here.
  // Or use some javascript concept that I'm not familiar with.
  events.hits -= 1 // Counting how many queries have been processed.
  if (events.hits == 0) {
    // All queries complete, time to draw the graph
    delete events.hits;
    drawGraph(events);
  }
};

/**
 * Updates the graph with data from a new list of API queries.
 *
 * @param {Array} events Array of event data
 */
function drawGraph(events) {
  var newData = {
    labels: getYearsArray(),
    datasets: []
  }
  
  var eventTypes = Object.keys(events);
  eventTypes.sort(); // Event types should be in alphabetical order
  var colors = getColors(eventTypes.length);
  
  for (var i=0; i<eventTypes.length; i++) {
    events[eventTypes[i]].borderColor = colors[i];
    events[eventTypes[i]].pointBackgroundColor = colors[i];
    events[eventTypes[i]].pointRadius=4;
    events[eventTypes[i]].pointHoverRadius=5;
    newData.datasets.push(events[eventTypes[i]]);
  }
  
  chart.data = newData;
  chart.update();
};


/**
 * Returns the list of selected options from the given selection
 *
 * @param {HTMLElement} selection
 */
function getSelectedValues(selection) {
  var result = [];
  var options = selection.options;
  var opt;
  for (var i=0; i<options.length; i++) {
    opt = options[i];
    if (opt.selected) {
      result.push([opt.label,opt.value]);
    }
  }
  return result;
}

/**
 * Returns an array of colors from the d3.interpolateCool color wheel.
 *
 * @param {Integer} queryCount The number of colors to return
 */
function getColors(queryCount) {
  var colorScale = d3.interpolateCool,
    intervalSize  = 1/queryCount,
    colors = [],
    i;
  for (i=0; i<queryCount; i++) {
    colors.push(colorScale(i*intervalSize));
  }
  return colors;
}


/**
 * When a user presses the "Update Graph" button, collects the selected options and updates the graph
 */
function newGraph() {
  var prosthetics,eventTypes;
  prosthetics = getSelectedValues(document.getElementById('ProsthesisTypes'));
  eventTypes = getSelectedValues(document.getElementById('EventTypes'));
  var urls = getURLs(prosthetics,eventTypes);
  makeQueries(urls);
}

/**
 * Returns a string representing today's date.
 *
 * @param {Boolean} yearOnly If true, return only the year portion of the date.
 */
function getTodayStr(yearOnly) {  
  var today = new Date();
  var dd = String(today.getDate()).padStart(2, '0');
  var mm = String(today.getMonth() + 1).padStart(2, '0');
  var yyyy = today.getFullYear();
  if (yearOnly) {
    return yyyy;
  }
  return yyyy+mm+dd;
}

/**
 * Returns an array of labels for the graph's x-axis.
 */
function getYearsArray() {
  // Year bounds for the graph
  var endYear = getTodayStr(true);
  var year;
  var years=[];
  for (year=2000; year<=endYear; year++) {
    if (year%5==0 || year==endYear){
      years.push(year);
    } else {
      years.push('');
    }
  }
  return years
}

/**
 * Returns an array of URLs to be queried.
 *
 * @param {Array} prosthetics The types of prosthetics to search
 * @param {Array} eventTypes The types of adverse events to search
 */
function getURLs(prosthetics,eventTypes) {
  var dateStr = getTodayStr(false)
  var url=[];
  var urls=[];
  var p,e; 
  for (var i in prosthetics) {
    for (var j in eventTypes) {
      p = prosthetics[i];
      e = eventTypes[j];
      url[0] = p[1]+': '+e[0];
      url[1] = 'https://api.fda.gov/device/event.json?search=device.generic_name:"Prosthesis"+AND+device.generic_name:"'+p[1]+'"+AND+event_type:"'+e[1]+'"+AND+date_of_event:[20000101+TO+'+dateStr+']&count=date_of_event';
      urls.push(url);
      url = [];
    }
  }
  return urls;
}

/**
 * Submits openFDA queries
 *
 * @param {Array} urls The URLs to query
 */
function makeQueries(urls) {
  events = {};
  events.hits = urls.length;
  for (url of urls) {
    assessment.fda_api(
      url,
      function(data) {
        _this.data = data;
        component.visualization.prototype.prep_data(container);
      }
    );
  }
}