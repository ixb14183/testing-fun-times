/**
 * Visualize the results of an API call to the FDA.
 */
component.visualization = function() {
	component.apply(this, arguments);
};
assessment.extend(component.visualization, component);

/**
 * Decorate.
 *
 * @param {HTMLDivElement} parent
 */
component.visualization.prototype.decorate = function(parent) {
	var self = this;
	var container = document.createElement('div');
	parent.appendChild(container);

	// Show basic loading text until the API call to the FDA completes.
	var loading = document.createElement('div');
	loading.innerText = 'Loading...';
	container.appendChild(loading);

	urls = [['Death','https://api.fda.gov/device/event.json?search=device.generic_name:"Prosthesis hip"+AND+event_type:"death"&count=date_of_event'],
			['Injury','https://api.fda.gov/device/event.json?search=device.generic_name:"Prosthesis hip"+AND+event_type:"injury"&count=date_of_event'],
			['Malfunction','https://api.fda.gov/device/event.json?search=device.generic_name:"Prosthesis hip"+AND+event_type:"malfunction"&count=date_of_event']];
	events = {};
	events.hits = 0;
	for (url of urls) {
		assessment.fda_api(
			url,
			events,
			function(data) {
				self.data = data;
				self.prep_data(container);
			}
		);
	}
};

/**
 * TODO
 *
 * @param {HTMLElement} parent
 */
component.visualization.prototype.prep_data = function(parent) {
	currentYear = parseInt(this.data[0].time.slice(0,4));
	eventCt = 0;
	if (typeof events['bounds'] == 'undefined') {
		firstYear = currentYear;
		finalYear = currentYear;
	} else {
		firstYear = Math.min(currentYear,events['bounds'][0]);
		finalYear = Math.max(currentYear,events['bounds'][1]);
	}
	
	// Loop through events. 
	// Events are returned in ascending date order, so we are able to keep count for the year as a single variable.
	for (i=0; i<this.data.length; i++) {
		day = this.data[i];
		year = parseInt(day.time.slice(0,4));
		if (year<firstYear) {
			firstYear = year;
		} else if (year>finalYear) {
			finalYear = year;
		}
		
		// This day is in a new year. Store the old year's data
		if (year!=currentYear) {
			if (typeof events[currentYear] == 'undefined') {
				events[currentYear] = [];
			}
			events[currentYear].push({type: type, count: eventCt});
			
			currentYear = year;
			eventCt = 0
		}
		
		// Wait to update the count in case this was the first day of the year
		eventCt += day.count;
	}
	
	// Save data for the final year of results from the query
	if (typeof events[currentYear] == 'undefined') {
		events[currentYear] = [];
	}
	events[currentYear].push({type: type, count: eventCt});
	events['bounds'] = [firstYear,finalYear];
	
	// This part feels sketchy and possibly subject to a race condition.
	// I tried to find a better way to wait until all queries were processed, but failed.
	events.hits += 1 // Counting how many queries have been processed.
	if (events.hits == 3) {
		// All queries complete, time to decorate
		component.visualization.prototype.decorate_data(parent, events);
	}
};

/**
 * TODO
 *
 * @param {HTMLElement} parent
 */
component.visualization.prototype.decorate_data = function(parent,events) {
	// To remove
	//console.log('DECORATE');
	//console.log(events);
	// End removal
	
	totals = {'Death': 0,'Injury': 0,'Malfunction': 0,'Grand': 0}
	
	htmlStr = '<pre>';
	//Build this later: parent.innerHTML = '<pre>' + JSON.stringify(events, null, '	') + '</pre>';
	
	table = document.createElement('table');
	for (y=events['bounds'][0]; y<=events['bounds'][1]; y++) {
		//console.log(y);
		if (typeof events[y] == 'undefined') {
			addRow(table,y,[],totals);			
		} else {
			addRow(table,y,events[y],totals);	
		}
	}
	finishTable(table,totals);
	buildTableHead(table);
	htmlStr += table.outerHTML;
	
	htmlStr += '</pre>';
	parent.innerHTML = htmlStr;
};

function buildTableHead(table) {
	var headers = ['Year','Death','Injury','Malfunction','Total'],
		//table = document.createElement('table'),
		thead = table.createTHead(),
		row = thead.insertRow();
	for (i=0; i<5; i++) {
		th = document.createElement('th');
		//console.log(headers[i]);
		th.appendChild(document.createTextNode(headers[i]));
		row.appendChild(th);
	}		
	//return table;
};

function addRow(table,year,data,totals) {
	// Format of data: {type: type, count: eventCt}
	// Collect the counts for each type of event
	
	var total = 0,
		death = 0,
		injury = 0,
		malfunction = 0;
	for (value in data) {
		total += data[value].count;
		if (data[value].type == 'Death') {
			death = data[value].count;
		} else if (data[value].type == 'Injury') {
			injury = data[value].count;
		} else {
			malfunction = data[value].count;
		}
		totals[data[value].type] += data[value].count
		totals['Grand'] += data[value].count
	}
	
	// Build the row
	row = table.insertRow();
	cell = row.insertCell();
	cellText = document.createTextNode(year);
	cell.appendChild(cellText);
	cell = row.insertCell();
	cellText = document.createTextNode(death);
	cell.appendChild(cellText);
	cell = row.insertCell();
	cellText = document.createTextNode(injury);
	cell.appendChild(cellText);
	cell = row.insertCell();
	cellText = document.createTextNode(malfunction);
	cell.appendChild(cellText);
	cell = row.insertCell();
	cellText = document.createTextNode(total);
	cell.appendChild(cellText);
};

function finishTable(table,totals) {
	// Build the final row - totals for each column
	row = table.insertRow();
	cell = row.insertCell();
	cellText = document.createTextNode('');
	cell.appendChild(cellText);
	cell = row.insertCell();
	cellText = document.createTextNode(totals['Death']);
	cell.appendChild(cellText);
	cell = row.insertCell();
	cellText = document.createTextNode(totals['Injury']);
	cell.appendChild(cellText);
	cell = row.insertCell();
	cellText = document.createTextNode(totals['Malfunction']);
	cell.appendChild(cellText);
	cell = row.insertCell();
	cellText = document.createTextNode(totals['Grand']);
	cell.appendChild(cellText);
};